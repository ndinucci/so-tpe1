#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include "getnum.h"
#include "defs.h"

static command srv_commands[SRV_CMD] = {{"verCartelera", verCartelera}, {"crearFuncion", crearFuncion }};


void verCartelera(){
	struct flock fl = {F_RDLCK, SEEK_SET, 0, 0, 0};
	int fd;
	fl.l_pid = getpid();
	char nombre[20];
	char codigo[5];
	char c;
	int i=0;
	int leer=1;
	int fnom=FALSE;
	
	fd=open("cartelera.txt",O_RDONLY);
	
	if(fd==-1){
		printf("\nNo hay funciones disponibles\n\n");
		return;	
	}
	
	fcntl(fd,F_SETLKW,&fl);
	
	if(!read(fd,&c,sizeof(char))){
		printf("\nNo hay funciones disponibles\n\n");
		return;	
	}
	while(leer!=0){
		if(c!='\n'){
			if(fnom){
				nombre[i]=c;						
			}
			else{
				codigo[i]=c;			
			}
			i++;
		}
		else{
			if(fnom){
				nombre[i]='\0';
				printf("Nombre de pelicula: %s\n",nombre);
				printf("\n---------------------------------------\n");
				while(getchar()!='\n');
				getchar();    
			}
			else{
				codigo[i]='\0';
				printf("\nCodigo de pelicula: %s\n",codigo);			
			}
			i=0;
			fnom=!fnom;
		}
		leer=read(fd,&c,sizeof(char));
											            
	}
	fl.l_type=F_UNLCK;
	fcntl(fd,F_SETLK,&fl);
}


void crearFuncion(){
	int fd;
	int i;
	int check;
	char codigo[5];
	char nombre[20];
	char buffer[11];
	char* aux;
	char asiento[MAX_COL+2];
	int row;
	int col;
	for(i=0;i<MAX_COL;i++){
		asiento[i]='0';
	}
	asiento[MAX_COL]='\0';
	struct flock fl = {F_WRLCK, SEEK_SET, 0, 0, 0};
	fl.l_pid = getpid();
	do{
		printf("Ingrese codigo de película\n");
		scanf("%4s",codigo);
		check = open(codigo, O_RDONLY);
		if(check!=-1){
			close(check);
			printf("El codigo ya existe\n");
		}
	}while(check!=-1);
	check = open(codigo,O_CREAT|O_WRONLY,0666);
	printf("Ingrese nombre de película\n");
	while(getchar()!='\n');
	scanf("%20s",nombre);
	while(getchar()!='\n');
	do{
		row = getint("Ingrese la cantidad de filas de la sala: ");
	}while(row>MAX_ROW || row<=0);
	do{
		col = getint("Ingrese la cantidad de columnas de la sala: ");
	}while(col>MAX_COL || col<=0);
	fcntl(check,F_SETLK,&fl);
	write(check,nombre,strlen(nombre));		//QUE ESCRIBA LO JUSTO Y NECESARIO
	write(check,"\n",1);
	aux=number_to_string(row,buffer);
	write(check,aux,strlen(aux));
	write(check,"\n",1);
	aux=number_to_string(col,buffer);
	write(check,aux,strlen(aux));
	write(check,"\n",1);
	asiento[col]='\n';
	asiento[col+1]='\0';
	for(i=0;i<row;i++){
		write(check,asiento,col+1);		//NO IMPRIMIR '/0'
	}
	fl.l_type=F_UNLCK;
	fcntl(check,F_SETLK,&fl);
	close(check);
	
	
	fd = open("cartelera.txt", O_WRONLY | O_APPEND);
	fl.l_type=F_WRLCK;
	fcntl(fd,F_SETLKW,&fl);
	write(fd, codigo, strlen(codigo));
	write(fd,"\n",1);
	write(fd, nombre, strlen(nombre));
	write(fd,"\n",1);
	fl.l_type=F_UNLCK;
	fcntl(fd,F_SETLK,&fl);
	close(fd);
}


void iniciarCartelera(void){
	int fd;
	fd=open("cartelera.txt",O_CREAT,0666);
	close(fd);
	
}


int main(){
	int i;
	char buffer[20];
	iniciarCartelera();
	while(TRUE){
		printf("Ingrese su comando : \n- ");
		scanf("%s",buffer);
		for(i=0;i<SRV_CMD;i++){
			if(!strcmp(buffer,srv_commands[i].name)){
				srv_commands[i].function();
				break;
			}
		}
		if(i==SRV_CMD){
			printf("Comando invalido\n");
		}
	}
}
