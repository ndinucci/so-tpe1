#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include "getnum.h"
#include "defs.h"

static command clt_commands[CLT_CMD] = {{"verCartelera", verCartelera}, {"verAsientosDisponibles", verAsientosDisponibles}, 										{"reservarAsientos", reservarAsientos}, {"verReserva", 										verReserva},{"confirmarReserva", confirmarReserva},
									{"cancelarReserva", cancelarReserva}};

static reserva res;

void cancelarReserva(){
	int fd,i,j,fila=0;
	char c;
	struct flock fl = {F_WRLCK, SEEK_SET, 0, 0, 0};
	char sala[MAX_ROW+3][MAX_COL+2]={0};
	if(res.usada==FALSE){
		printf("\nUsted no tiene reserva\n");
		return;
	}
	fd=open(res.codigoFuncion,O_RDWR);
	if(fd==-1){
		printf("\nLa funcion de su reserva ha sido cancelada\n");
		res.usada=FALSE;
		return;	
	}
	fcntl(fd,F_SETLKW,&fl);
	i=0;
	j=0;
	while(read(fd,&c,1)){
		sala[i][j]=c;
		if(c=='\n'){
			i++;
			j=0;
		}else{
			j++;
		}
	}
	for(i=0;i<res.cantAsientos;i++){
		sala[res.asientos[i]/100+3][res.asientos[i]%100]='0';	
	}
	i=0;
	while(sala[1][i]!='\n'){
		fila*=10;
		fila+=sala[1][i]-'0';
		i++;
	}
	lseek(fd,0,SEEK_SET);
	for(i=0;i<fila+3;i++){
		write(fd,sala[i],strlen(sala[i]));
	}
	fl.l_type=F_UNLCK;
	fcntl(fd,F_SETLK,&fl);
	close(fd);
	res.usada=FALSE;

}


void reservarAsientos(){		
	int fd;
	char c;
	int i=0,j,invalido,fila,columna;
	char sala[MAX_ROW+3][MAX_COL+2]={0};
	int check;
	struct flock fl = {F_WRLCK, SEEK_SET, 0, 0, 0};
	fl.l_pid = getpid();	
	if(res.usada!=FALSE){
		check = open(res.codigoFuncion, O_RDONLY);
		if(check==-1){
			res.usada=FALSE;
		}else{
			close(check);
			printf("\nYa tiene una reserva actualmente, solo puede tener una activa\n");	
			return;		
		}
	}
	printf("\nIngrese el codigo de la pelicula para la cual desea reservar asientos\n");
	scanf("%4s",res.codigoFuncion);
	fd = open(res.codigoFuncion,O_RDWR);
	if(fd == -1){
		printf("\nNo existe una pelicula con el codigo indicado\n");
		printf("Para ver los codigos de las peliculas disponibles, use la funcion verCartelera\n");
		return;
	}
	do{
		res.cantAsientos=getint("Elija la cantidad de asientos a reservar:(max %d)\n",MAX_SIT);
	}while(res.cantAsientos>MAX_SIT || res.cantAsientos<1);
	for(i=0;i<res.cantAsientos;i++){
		res.asientos[i]=100*getint("asiento %d: \n-elija la fila:  ",i+1);
		res.asientos[i]+=getint("-elija la columna:  ");
	}
	fcntl(fd,F_SETLKW,&fl);
	i=0;
	j=0;
	while(read(fd,&c,1)){
		sala[i][j]=c;
		if(c=='\n'){
			i++;
			j=0;
		}else{
			j++;
		}
	}
	fila=0;
	columna=0;
	i=0;
	while(sala[1][i]!='\n'){
		fila*=10;
		fila+=sala[1][i]-'0';
		i++;
	}
	i=0;
	while(sala[2][i]!='\n'){
		columna*=10;
		columna+=sala[2][i]-'0';
		i++;
	}
	for(i=0,invalido=0;i<res.cantAsientos && invalido==0;i++){
		if( !(res.asientos[i]/100>=0 && res.asientos[i]%100>=0 && res.asientos[i]/100<fila && 
			res.asientos[i]%100<columna) || sala[res.asientos[i]/100+3][res.asientos[i]%100]=='X'){
			invalido=1;
		}else{
			sala[res.asientos[i]/100+3][res.asientos[i]%100]='X';
		}
	}
	if(invalido==1){
		fl.l_type=F_UNLCK;
		fcntl(fd,F_SETLK,&fl);
		close(fd);
		printf("Alguno de los asientos elegidos esta ocupado o no existe\n");
		return;
	}
	lseek(fd,0,SEEK_SET);
	for(i=0;i<fila+3;i++){
		write(fd,sala[i],strlen(sala[i]));
	}
	fl.l_type=F_UNLCK;
	fcntl(fd,F_SETLK,&fl);
	close(fd);
	res.usada=TRUE;
	return;
}


void verAsientosDisponibles(){
	char codigo[5];
	int fd;
	int leer;
	int i=0;
	char c;
	struct flock fl = {F_RDLCK, SEEK_SET, 0, 0, 0};
	fl.l_pid = getpid();
	printf("\nIngrese el codigo de la pelicula\n");
	scanf("%4s",codigo);
	fd = open(codigo,O_RDONLY);
	if(fd == -1){
		printf("\nNo existe una pelicula con el codigo indicado\n");
		printf("Para ver los codigos de las peliculas disponibles, use el comando verCartelera\n");
		return;
	}
	printf("\nAsientos disponibles:\n(0: disponible - X: ocupado)\n\n");
	fcntl(fd,F_SETLKW,&fl);
	while(i!=3){
		read(fd,&c,1);
		if(c=='\n'){
			i++;
		}
	}
	while(read(fd,&c,1)!=0){
		putc(c,stdout);
	}
	printf("\np a n t a l l a \n");
	fl.l_type=F_UNLCK;
	fcntl(fd,F_SETLK,&fl);
	close(fd);
}


void verCartelera(){
	struct flock fl = {F_RDLCK, SEEK_SET, 0, 0, 0};
	int fd;
	fl.l_pid = getpid();
	char nombre[20];
	char codigo[5];
	char c;
	int i=0;
	int leer=1;
	int fnom=FALSE;
	
	fd=open("cartelera.txt",O_RDONLY);
	
	if(fd==-1){
		printf("\nNo hay funciones disponibles\n\n");
		return;	
	}
	
	fcntl(fd,F_SETLKW,&fl);
	
	if(!read(fd,&c,sizeof(char))){
		printf("\nNo hay funciones disponibles\n\n");
		return;	
	}
	while(leer!=0){
		if(c!='\n'){
			if(fnom){
				nombre[i]=c;						
			}
			else{
				codigo[i]=c;			
			}
			i++;
		}
		else{
			if(fnom){
				nombre[i]='\0';
				printf("Nombre de pelicula: %s\n",nombre);
				printf("\n---------------------------------------\n");
				while(getchar()!='\n');
				getchar();    
			}
			else{
				codigo[i]='\0';
				printf("\nCodigo de pelicula: %s\n",codigo);			
			}
			i=0;
			fnom=!fnom;
		}
		leer=read(fd,&c,sizeof(char));
											            
	}
	fl.l_type=F_UNLCK;
	fcntl(fd,F_SETLK,&fl);
}


void confirmarReserva(){
	if(res.usada==FALSE){
		printf("\nUsted no tiene reserva\n");
	}
	else{
		printf("\nSu reserva ha sido confirmada con exito\n");	
		res.usada=FALSE;
	}	
	return;
}


void verReserva(){
	int i;
	int check;
	if(res.usada!=FALSE){
		check = open(res.codigoFuncion, O_RDONLY);
		if(check==-1){
			printf("La funcion para la cual tenia reserva fue cerrada\n");
			res.usada=FALSE;
			return;
		}else{
			close(check);
		}

		printf("\nSu reserva es para la funcion de codigo %s\n",res.codigoFuncion);
		printf("y ha reservado %d asientos.\n",res.cantAsientos);
		printf("Los mismos son:\n");
		for(i=0;i<res.cantAsientos;i++){
			printf("asiento: %d,  fila: %d, columna: %d.\n", i+1, res.asientos[i]/100,res.asientos[i]%100);
		}
	}else{
		printf("\nUsted no tiene reserva.\n");
	}
}


int main(){
	int i;
	char buffer[20];
	res.usada=FALSE;
	while(TRUE){
		printf("\nIngrese su comando : \n- ");
		scanf("%s",buffer);
		for(i=0;i<CLT_CMD;i++){
			if(!strcmp(buffer,clt_commands[i].name)){
				clt_commands[i].function();
				break;
			}
		}
		if(i==CLT_CMD){
			printf("\nComando invalido\n");
		}
	}
}





