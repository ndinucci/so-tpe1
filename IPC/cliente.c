#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include "getnum.h"
#include "defs.h"

#include <errno.h>

static key_t keyout=0xBEEF1;
static command clt_commands[CLT_CMD] = {{"verCartelera", verCartelera}, {"verAsientosDisponibles", verAsientosDisponibles}, 										{"reservarAsientos", reservarAsientos}, {"verReserva", 										verReserva},{"confirmarReserva", confirmarReserva},
									{"cancelarReserva", cancelarReserva}};


int logueado=0;
int leer=0;
int escribir=0;
reserva res;

void verCartelera(){
	char c=' ';
	printf("\n");
	write(escribir,"1",1);
	while( c!='\0'){
		read(leer,&c,1);
		if(c!='\0' && (isalpha(c)||c==' '||c=='\n'||isdigit(c))){
			printf("%c",c);
		}	
	}
}


void verAsientosDisponibles(){
	char codigo[5];
	char c;
	printf("\nIngrese el codigo de la pelicula\n");
	scanf("%4s",codigo);
	write(escribir,"2",1);
	write(escribir,codigo,4);
	read(leer,&c,1);
	if(c=='0'){ 
		printf("Codigo de funcion invalido\n");
		return;
	}	
	printf("\nAsientos disponibles:\n(0: disponible - X: ocupado)\n\n");
	while(c!='\0'){	
		read(leer,&c,1);
		printf("%c",c);
	
	}
	printf("\np a n t a l l a \n");
}

void reservarAsientos(){
	char c;
	int i;
	write(escribir,"3",1);
	if(res.usada!=FALSE){
		printf("\nYa tiene una reserva actualmente, solo puede tener una activa\n");	
		return;		
	}
	printf("\nIngrese el codigo de la pelicula para la cual desea reservar asientos\n");
	scanf("%4s",res.codigoFuncion);
	do{
		res.cantAsientos=getint("Elija la cantidad de asientos a reservar:(max %d)\n",MAX_SIT);
	}while(res.cantAsientos>MAX_SIT || res.cantAsientos<1);
	for(i=0;i<res.cantAsientos;i++){
		res.asientos[i]=100*getint("asiento %d: \n-elija la fila:  ",i+1);
		res.asientos[i]+=getint("-elija la columna:  ");
	}
	write(escribir,res.codigoFuncion,4);
	read(leer,&c,1);
	if(c!='1'){
		printf("\nLa funcion no existe\n");
		return;
	}
	c=res.cantAsientos;
	write(escribir,&c,1);
	for(i=0;i<res.cantAsientos;i++){
		c=res.asientos[i]/100;
		write(escribir,&c,1);
		c=res.asientos[i]%100;
		write(escribir,&c,1);
	}
	read(leer,&c,1);
	if(c=='1'){
		res.usada=TRUE;
		printf("\nReserva realizada con exito\n");	
	}
	else{
		printf("\nAlguno de los asientos elegidos esta ocupado o no es valido\n");	
	}
}

void verReserva(){
	int i;
	if(res.usada!=FALSE){
		printf("\nSu reserva es para la funcion de codigo %s\n",res.codigoFuncion);
		printf("y ha reservado %d asientos.\n",res.cantAsientos);
		printf("Los mismos son:\n");
		for(i=0;i<res.cantAsientos;i++){
			printf("asiento: %d,  fila: %d, columna: %d.\n", i+1, res.asientos[i]/100,res.asientos[i]%100);
		}
	}else{
		printf("\nUsted no tiene reserva.\n");
	}
}

void confirmarReserva(){
	if(res.usada==FALSE){
		printf("\nUsted no tiene reserva.\n");
	}
	else{
		printf("\nSu reserva ha sido confirmada con exito\n");	
		res.usada=FALSE;
	}	
	return;
}

void cancelarReserva(){
	char c;
	int i;
	if(res.usada==FALSE){
		printf("\nUsted no tiene reserva\n");
		return;
	}
	write(escribir,"5",1);
	write(escribir,res.codigoFuncion,4);
	c=res.cantAsientos;
	write(escribir,&c,1);
	for(i=0;i<res.cantAsientos;i++){
		c=res.asientos[i]/100;
		write(escribir,&c,1);
		c=res.asientos[i]%100;
		write(escribir,&c,1);
	}
	read(leer,&c,1);
	if(c=='1'){
		printf("\nReserva cancelada con exito\n");
	}
	else{
		printf("\nLa funcion de su reserva ha sido cancelada\n");
	}
	res.usada=FALSE;
}

void login(){
	logueado=1;
}

int main(){
	int msgqid=0;
	char aux[15];
	char pidc[13];
	char pids[13];
	int c;
	int i;
	struct{
		long mtype;
		char mtext[10];
	}msg;	
	int pid=getpid();
	res.usada=FALSE;
	sprintf(aux, "%d", pid);
	memcpy(msg.mtext,aux,6);
	sprintf(pidc, "/tmp/%dc", pid);
	sprintf(pids, "/tmp/%ds", pid);
	if(mknod(pidc,S_IFIFO|0666,0)==-1){
		printf("Error creando el FIFO: %s\n",pidc);
		return -1;
	}
	signal(LOGSIG,&login);
	msgqid=msgget(keyout,IPC_CREAT|0666);
	if(msgqid==-1){
		printf("Error Fatal msqid\n");
		return -1;
	}
	msgsnd(msgqid,&msg,sizeof(msg.mtext),0);
	while(!logueado){
		pause();
	}
	printf("Bienvenido\n");
	escribir=open(pidc,O_WRONLY);
	leer=open(pids,O_RDONLY);
	if(leer==-1 || escribir==-1){
		printf("Error abriendo los FIFOs");
		return -1;
	} 
	while(TRUE){
		printf("\nIngrese su comando : \n- ");
		scanf("%s",aux);
		for(i=0;i<CLT_CMD;i++){
			if(!strcmp(aux,clt_commands[i].name)){
				clt_commands[i].function();
				break;
			}
		}
		if(i==CLT_CMD){
			printf("\nComando invalido\n");
		}
	}
	return 0;
}


