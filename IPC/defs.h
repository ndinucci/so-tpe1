
#define SRV_CMD 4
#define CLT_CMD 6
#define MAX_COL 20
#define MAX_ROW 10
#define TRUE 1
#define FALSE 0
#define MAX_SIT 10
#define LOGSIG 5
#define CANTFUNC 4




typedef struct{
	char* codigoFuncion;
	char* nombre;
	int  dimFilas;
	int  dimColumnas;
	char sala[MAX_ROW][MAX_COL];
}funcion;



typedef void (*func)(void);

typedef struct{
	char* name;
	func  function;

} command;

typedef struct{
	char codigoFuncion[5];
	int cantAsientos;
	int asientos[MAX_SIT];                   /*( FILA*100 + COLUMNA)*/
	int usada;
} reserva;


void verAsientosDisponibles();
void reservarAsientos();
void verReserva();
void confirmarReserva();
void cancelarReserva();


void verCartelera();

	/* ABRIR ARCHIVO DE CARTELERA ( si no existe o esta vacio decir que no hay funciones)
	   LISTAR LOS CODIGOS Y NOMBRES DE LAS FUNCIONES*/



void crearFuncion();

	/* PEDIR NOMBRE, CODIGO , DIMFILA , DIMCOL
	   VER QUE NO EXISTA OTRA FUNCION CON ESE CODIGO (informar si ya existe y retornar)
	   CREAR EL ARCHIVO DE LA FUNCION Y AGREGARLA A LA CARTELERA
	*/




void cerrarFuncion();

	/* PEDIR EL CODIGO DE LA FUNCION A CERRAR
	   SI NO EXISTE NO HACE NADA
	   SI EXISTE BORRAR EL ARCHIVO DE LA FUNCION Y BORRAR DE LA CARTELERA
	*/


